﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InterviewQuizTests
{
    public class UnitTest1
    {
        protected delegate int AtoiFunction(String str);
        protected AtoiFunction MethodToTest;

        public UnitTest1()
        {

            //You must comment out the Atoi implementation you don't want to test

            //code during interview
            //this.MethodToTest = InterviewQuiz.Program.AtoiDuringInterview;

            //code after interview 
            this.MethodToTest = InterviewQuiz.Program.AtoiAfterInterview;
        }

    }

    [TestClass]
    public class BadInputUnitTests : UnitTest1
    {

        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void Atoi_LessThanMin_ShouldThrow()
        {
            //arrange
            String input = "-2147483649";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void Atoi_MuchLessThanMin_ShouldThrow()
        {
            //arrange
            String input = "-21474836494654654564564654465464654654654654231231564";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void Atoi_GreaterThanMax_ShouldThrow()
        {
            //arrange
            String input = "2147483648";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void Atoi_GreaterThanMaxWithSign_ShouldThrow()
        {
            //arrange
            String input = "+2147483648";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }


        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void Atoi_MuchGreaterThanMax_ShouldThrow()
        {
            //arrange
            String input = "21474836494654654564564654465464654654654654231231564";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void Atoi_MuchGreaterThanMaxWithSign_ShouldThrow()
        {
            //arrange
            String input = "+21474836494654654564564654465464654654654654231231564";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Atoi_CharsInTheBeginning_ShouldThrow()
        {
            //arrange
            String input = "a123";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Atoi_CharsInTheMiddle_ShouldThrow()
        {
            //arrange
            String input = "12W67";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Atoi_CharsInTheEnd_ShouldThrow()
        {
            //arrange
            String input = "12345A";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Atoi_JustPlusSign_ShouldThrow()
        {
            //arrange
            String input = "+";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Atoi_JustMinusSign_ShouldThrow()
        {
            //arrange
            String input = "-";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Atoi_EmptyString_ShouldThrow()
        {
            //arrange
            String input = "";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Atoi_JustWhitespace_ShouldThrow()
        {
            //arrange
            String input = "                           ";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Atoi_WhitespaceInTheBeginning_ShouldThrow()
        {
            //arrange
            String input = " +54";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Atoi_WhitespaceInTheMiddle_ShouldThrow()
        {
            //arrange
            String input = "54 45";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Atoi_WhitespaceInTheEnd_ShouldThrow()
        {
            //arrange
            String input = "567 ";

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Atoi_NullArgument_ShouldThrow()
        {
            //arrange
            String input = null;

            //act
            int result = this.MethodToTest(input);

            //assert is handled by ExpectedException
        }

    } //BadInputUnitTests




    [TestClass]
    public class GoodInputUnitTests : UnitTest1
    {

        [TestMethod]
        public void Atoi_MinimumInt_ShouldSucceed()
        {
            //arrange
            String input = "-2147483648";
            int expect = -2147483648;

            //act
            int result = this.MethodToTest(input);

            //assert
            Assert.AreEqual(expect, result);
        }

        [TestMethod]
        public void Atoi_Minus12345_ShouldSucceed()
        {
            //arrange
            String input = "-12345";
            int expect = -12345;

            //act
            int result = this.MethodToTest(input);

            //assert
            Assert.AreEqual(expect, result);
        }

        [TestMethod]
        public void Atoi_MinusOne_ShouldSucceed()
        {
            //arrange
            String input = "-1";
            int expect = -1;

            //act
            int result = this.MethodToTest(input);

            //assert
            Assert.AreEqual(expect, result);
        }

        [TestMethod]
        public void Atoi_MinusZero_ShouldSucceed()
        {
            //arrange
            String input = "-0";
            int expect = 0;

            //act
            int result = this.MethodToTest(input);

            //assert
            Assert.AreEqual(expect, result);
        }

        [TestMethod]
        public void Atoi_Zero_ShouldSucceed()
        {
            //arrange
            String input = "0";
            int expect = 0;

            //act
            int result = this.MethodToTest(input);

            //assert
            Assert.AreEqual(expect, result);
        }

        [TestMethod]
        public void Atoi_PlusZero_ShouldSucceed()
        {
            //arrange
            String input = "+0";
            int expect = 0;

            //act
            int result = this.MethodToTest(input);

            //assert
            Assert.AreEqual(expect, result);
        }

        [TestMethod]
        public void Atoi_Nine_ShouldSucceed()
        {
            //arrange
            String input = "9";
            int expect = 9;

            //act
            int result = this.MethodToTest(input);

            //assert
            Assert.AreEqual(expect, result);
        }

        [TestMethod]
        public void Atoi_PlusNine_ShouldSucceed()
        {
            //arrange
            String input = "+9";
            int expect = 9;

            //act
            int result = this.MethodToTest(input);

            //assert
            Assert.AreEqual(expect, result);
        }

        [TestMethod]
        public void Atoi_12345_ShouldSucceed()
        {
            //arrange
            String input = "12345";
            int expect = 12345;

            //act
            int result = this.MethodToTest(input);

            //assert
            Assert.AreEqual(expect, result);
        }

        [TestMethod]
        public void Atoi_Plus12345_ShouldSucceed()
        {
            //arrange
            String input = "+12345";
            int expect = 12345;

            //act
            int result = this.MethodToTest(input);

            //assert
            Assert.AreEqual(expect, result);
        }

        [TestMethod]
        public void Atoi_MaximumInt_ShouldSucceed()
        {
            //arrange
            String input = "2147483647";
            int expect = 2147483647;

            //act
            int result = this.MethodToTest(input);

            //assert
            Assert.AreEqual(expect, result);
        }

        [TestMethod]
        public void Atoi_PlusMaximumInt_ShouldSucceed()
        {
            //arrange
            String input = "+2147483647";
            int expect = 2147483647;

            //act
            int result = this.MethodToTest(input);

            //assert
            Assert.AreEqual(expect, result);
        }


    } //GoodInputUnitTests

} //namespace
