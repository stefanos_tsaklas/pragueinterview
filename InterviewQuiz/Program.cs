﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewQuiz
{
    public class Program
    {
        private static int Add(int a, int b)
        {
            return checked(a + b);
        }

        private static int Sub(int a, int b)
        {
            return checked(a - b);
        }

        delegate int Operation(int a, int b);

        //Atoi can convert all numbers in range [-2147483648, 2147483647]
        //Numbers outside of this range with throw an Overflow exception
        //Positive numbers may or may have not a + sign
        //No non-numeric characters are allowed anywhere in the input
        //No whitespace is allowed anywhere in the input
        public static int AtoiDuringInterview(String str)
        {
            //validation, throw an exception for invalid input
            CheckNumericInput(str);

            //parse the sign of the number, and decide the starting point 
            char[] input = str.ToCharArray();
            int sign  = (input[0] == '-') ? -1 : +1;
            int start = (input[0] == '-' || input[0] == '+') ? +1 : 0;

            int sum = 0;
            for (int i = start; i < input.Length; i++)
            {
                sum = sum * 10 + (input[i]-'0');

                //detect overflow if we added something and sum is now negative
                if (sum < 0)
                    throw new OverflowException("Overflow exception");
            }

            //we may need to flip the sign of the result
            return sign*sum;
        }

        private static void CheckNumericInput(String str)
        {
            //reject empty input
            if (str == null || str.Trim().Length == 0)
                throw new ArgumentException("Input is not numeric");

            //decide beginning of digits
            int start = (str[0] == '-' || str[0] == '+') ? +1 : 0;
           
            //check that all characters are digits
            foreach(char c in str.Substring(start))
            {
                if ( !(c >= '0' && c <= '9'))
                    throw new ArgumentException("Input is not numeric");
            }
        }

        public static int AtoiAfterInterview(String str)
        {
            //reject empty input
            if (str == null || str.Trim().Length == 0)
                throw new ArgumentException("Invalid input");

            //TODO: maybe we can proactively reject long input, longer than 11 characters, without even examining the characters

            //parse the sign of the number, and the starting point 
            char[] input = str.ToCharArray();
            int sign = (input[0] == '-') ? -1 : +1;
            Operation function = ((sign > 0) ? (Operation)Add : (Operation)Sub);
            int start = (input[0] == '-' || input[0] == '+') ? +1 : 0;

            //handle the special input case of either "+" or "-"
            if (start != 0 && input.Length == 1)
                throw new ArgumentException("Invalid input");

            int sum = 0;
            for (int i = start; i < input.Length; i++)
            {
                char c = input[i];
                if (c < '0' || c > '9')
                    throw new ArgumentException("Input '{0}' contains non-numeric characters", str);
                else
                {
                    //this may add or subtract to the sum
                    sum = function(sum * 10, c - '0');
                }
            }

            return sum;
        }

        static void Main(string[] args)
        {
            /*
            Console.WriteLine("max value is {0}", int.MaxValue);
            string[] inputs = { int.MinValue.ToString(), (int.MinValue + 1).ToString(), "-123", "-1", "-0", "0", "+0", "1", "+1", "1337", "+1337", "650000", (int.MaxValue - 1).ToString(), int.MaxValue.ToString(), "2147483648", "2147483649", "5456456454564564456", "a123", "12a3", "123a", "+", "-", "", "    ", null };
            foreach (string str in inputs)
            {
                try
                {
                    Console.WriteLine("Atoi({0}) = {1} ", str, AtoiAfterInterview(str));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Could not convert '{0}' to int ({1})", str, ex.Message);
                }

            }
            Console.Read();
             */
        }
    }
}
